/*Vue.component('todo-item', {
	props: ['todo'],                                                                                   <!-- v-on:change="todo.display = !todo.display" mais pas de timeout compatible Vue.js trouvé -->
	template: '<li><input type="checkbox" class="check" id="todotask" name="todotask" v-on:change="todo.ended = !todo.ended" v-if="todo.display"><label for="todotask" v-if="todo.display">{{ todo.text }}</label></li>'
});*/

var app = new Vue({
	el: "#app",
	data: {
		todos: [
			{ id: 0, display: true, ended: false, text: "Réviser pour les partiels" },
			{ id: 1, display: true, ended: false, text: "Se renseigner sur Vue.js" },
			{ id: 2, display: true, ended: false, text: "Créer cette mini-page en Vue.js" },
			{ id: 3, display: true, ended: false, text: "La montrer à M. Bachatene" },
			{ id: 4, display: true, ended: false, text: "Obtenir un stage" }
		],
		message: "Liste des choses à faire:",
		date: "Liste ouverte le " + new Date().toLocaleString(),
		toAdd: "",
		nbElements: 5,
	},
	
	methods: {
		addToList: function() {
			if(this.toAdd != ""){ 
				this.todos.push({ id: this.nbElements, display: true, ended: false, text: this.toAdd });
				this.nbElements = this.nbElements + 1;
				this.toAdd = "";
			}
		},

		removeFromList: function() {
			for(let i=0 ; i < this.nbElements ; i++) {
				if(this.todos[i].ended) {
					// erreur: la checkbox n'est "pas supprimée" s'il y a des éléments après => la checkbox de l'index i reste checked donc pb d'affichage pour l'élément i+1
					// mise en place d'un tri pour que les évènements terminés soient à la fin de la liste
					
					// this.todos[i].ended = false;
					// this.todos.splice(i, 1);
					
					this.todos[i].display = false;
				}
			}
		},
		
		sort: function() {
		/* FAIRE LE TRI DANS L'ORDRE => REMONTER LES TACHES PLUTOT QUE SWAP */
			var temp;
			var last;
			
			// pas sûr que ça marche
			for(let i=0 ; i < this.nbElements ; i++) {
				var j = nbElements-1;
				while(this.todos[j].ended){
					j = j-1;
					last = this.todos[j];
				}
				if(this.todos[i].ended) {
					temp = last;
					this.todos[j] = this.todos[i];
					this.todos[i] = temp;
				}
			}
		}
	}
});